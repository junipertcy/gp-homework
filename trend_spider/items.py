# Models for scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class OzoneItem(Item):
    _id = Field()
    date = Field()
    loc = Field()
    value = Field()

    pass

