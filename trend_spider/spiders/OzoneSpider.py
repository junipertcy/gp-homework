import numpy as np
import scrapy
import os
import re
from scrapy.spiders import CrawlSpider
from trend_spider.items import OzoneItem
import pendulum


class OzoneTrendSpider(CrawlSpider):
    name = "ozoneSpider"
    allowed_domains = ["acd-ext.gsfc.nasa.gov"]

    start_urls = []
    years = list(map(lambda x: str(int(x))[2:], np.linspace(2005, 2016, 12)))
    months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
    # months = ["jan"]
    # years = ["05"]
    template = 'https://acd-ext.gsfc.nasa.gov/Data_services/cloud_slice/data_monthly/L3_tropo_ozone_vmr_'

    for year in years:
        for month in months:
            start_urls += [template + month + year]

    def __init__(self):
        pass

    def parse(self, response):
        ozone_item = OzoneItem()

        sel = scrapy.selector.Selector(response=response)
        text = ''.join(sel.xpath("//body//text()").extract()).strip()

        # Pre-process the text data, so that it is readable into scrapy.Items.
        parsed_text_ = "%".join(
            list(
                map(
                    # strip() will not work, as some values have only two digits.
                    lambda x: x[1:], "".join(text.split("steps)")[2:]).split("\n")[1:]

                )
            )
        )
        parsed_text = re.sub('\s+', ' ', parsed_text_) + "%"

        data_array = np.zeros((120, 288))

        lat_s = np.linspace(-59.5, 59.5, 120)
        lng_s = np.linspace(-179.375, 179.375, 288)
        splitter = list(map(lambda x: " lat = " + str(x) + "%", lat_s))

        parsed_text_lat_list = list()
        for idx, s in enumerate(splitter):
            text_list = parsed_text.split(s)
            appended_text = text_list[0]
            parsed_text_lat_list += [appended_text]
            text_list = text_list[1:]
            parsed_text = "".join(text_list)

        for lat in range(0, 120):
            lng = 0
            string_to_push = ""
            counter = 0
            for idx, char in enumerate(parsed_text_lat_list[lat]):
                if char != "%":
                    counter += 1
                    if char == " ":
                        char = ""
                    string_to_push += char

                    if counter != 1 and counter % 3 == 0:
                        data_array[lat][lng] = int(string_to_push)
                        data_time = response.request.url[-5:]
                        data_month = data_time[:3]
                        data_year = "20" + data_time[-2:]
                        data_time = pendulum.parse(data_month + data_year)

                        ozone_item["date"] = data_time

                        ozone_item["loc"] = {
                            "type": "Point",
                            "coordinates": [float(lng_s[lng]), float(lat_s[lat])]
                        }

                        ozone_item["value"] = int(string_to_push)

                        lng += 1
                        string_to_push = ""
                        counter = 0
                        yield ozone_item
