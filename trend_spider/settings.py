# Scrapy settings for the greenpeace ozone trend analysis project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
import os

BOT_NAME = 'trend_spider'

SPIDER_MODULES = ['trend_spider.spiders']
NEWSPIDER_MODULE = 'trend_spider.spiders'

# Item pipeline
ITEM_PIPELINES = {
    "trend_spider.pipeline.MongoPipeline": 1000
}


MONGO_URI = 'localhost:27017'
MONGO_DATABASE = 'greenpeace'
MONGODB_COLLECTION = 'ozone'
MONGODB_UNIQ_KEY = '_id'

#

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'trend_spider (+http://www.junipertcy.info)'

# Enable auto throttle
AUTOTHROTTLE_ENABLED = True

COOKIES_ENABLED = False

# Set your own download folder
DOWNLOAD_FILE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "download_file")


