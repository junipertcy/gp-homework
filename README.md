### A pipeline for the homework
The link below has NASA lower atmosphere (troposphere) ozone data derived from satellite observations. Download the whole time series of monthly global data (in parts per billion, the lower list), process into maps and analyse the trend of summertime (May-August) ozone over the period. Which countries/regions have seen the largest increases and decreases? You can use any software or programming language you like, but please be prepared to share your workflow/code as well as the result.
https://acd-ext.gsfc.nasa.gov/Data_services/cloud_slice/new_data.html


#### Get countries geojson data
1. Get geojson data:
    ```commandline
    git clone https://github.com/johan/world.geo.json.git
    ```

2. manually modify the file, so it can be transferred with `mongoimport` with ease.
    ```commandline
    mongoimport --db greenpeace --collection countries --type json --file countries.geo.json --jsonArray
    ```

3. Enter the mongodb shell, create indexes for faster queries.
    ```
    use greenpeace
    db.countries.createIndex({geometry: "2dsphere"})
    ```

#### Get the ozone concentration data
1. Run the scrapy crawler to get all the data.
    ```commandline
    scrapy runspider trend_spider/spiders/OzoneSpider.py
    ```
    
2. Create indexes for the collection
    ```commandline
    use greenpeace
    db.ozone.createIndex({loc: "2dsphere"})
    ```

#### Post-processings

* Goal I. 
   > Get the summer time ozone data, across the 12-year range, and grouped by country.
     So we could reduce the whole dataset to managable sizes.
     
1. Annotate the grids in `ozone` collection with a `tag`, specifying its associated country.
   The script is written in `trend_analyzer/annotate_country_tags.ipynb`
    
2. Process the data in the `trend_analyzer/parse_data.ipynb` notebook.

3. By drawing lines (and extracting its regression slope!) whose composing points indicate monthly data in a moving window, we suggest that:
    * countries that have seen the largest ozone increases: North Korea, Tajikistan, Haiti
    * countries that have seen the largest ozone decreases: French Southern and Antarctic Lands, Chile, Norway
    
* Goal II. 
   > Get the summer time ozone data, across the 12-year range, and grouped by (longitude, latitude).
     So we could reduce the whole dataset to managable sizes.

1. Use the `ozone` collection after Step I-1.

2. Process the data in the `trend_analyzer/parse_data.ipynb` notebook (lower half).

3. We find some regions, not being enclosed by countries, exhibit rather large increase and decrease of summertime ozone levels.
We plot the top regions that show larger increase on a map, to show where they are, in file `draw_map.ipynb`. 

#### Mapping
We use [Carto](https://carto.com/) to visualize the geo-referenced data.
We could do so in Python, but since our data set is rather _numerous_ (but not large), it's slower.
The csv data (e.g. `dataset/may05.csv`) is a file that can be imported to `Carto` directly.

> Visit: https://junipertcy.carto.com/viz/29fb57f3-6886-4cf9-8abf-9ad8cec276dd/public_map for a glimpse of the distribution.

As we visualized the map, we could see the heterogeneities of the ozone concentration distribution.
Notably, in the oceanic regions that we failed to tag them with countries, we could also see where it might show particularities.

